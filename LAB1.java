
public class LAB1 {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		circle circle1 = new circle();
		
		System.out.println("    State----X--------Y-------R---");
		
		circle1.setX(-2.06);
        circle1.setY(-0.23);
        circle1.setR(1.33);
        printer(1,circle1);
        
        circle1.setX(0.0);
        circle1.setY(5.0);
        circle1.setR(1.88);
        printer(2,circle1);
        
        
        circle1.translate(5.0, 0.0);
        printer(3,circle1);
        circle1.scale(0.5);
        printer(4,circle1);
        circle1.translate(-1, -1);
        circle1.setR(3.1);
        printer(5,circle1);
        System.out.println("Area: "+circle1.getArea());
	}

	
	public static void printer(int state, circle Circle){
		//System.out.println("-------------------------------");
		System.out.println("     "+state+"      "+Circle.getX()+"      "+Circle.getY()+"      "+Circle.getR());
		System.out.println("    -----------------------------------");
			}
}
