
public class circle {
	
	public double x;
	public double y;
	public double radious;
	
	public double getX() {
    	return this.x;
    }
    
	public void setX(double newX) {
		this.x=newX;
    }
    
	public double getY() {
    	return this.y;
    }
        
	public void setY(double newY) {
    	this.y=newY;
    }
    
	public double getR() {
    	return this.radious;
    }
        
	public void setR(double newR) {
    	this.radious = newR;
    }
    
	public double getArea() {
    	return this.radious*this.radious*Math.PI;
    }
	
	public void translate(double dx,double dy) {
    	this.x+=dx;
    	this.y+=dy;
    }
	
	public double scale(double scaler) {
    	this.radious*=scaler;
    	return this.radious;
    }
    
}
